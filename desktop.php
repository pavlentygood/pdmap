<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Карта ПД</title>
	<link rel="stylesheet" href = "style.css" type = "text/css">
	<link rel="stylesheet" href = "desktop_style.css" type = "text/css">
	<script type ="text/javascript" src = "jquery.js"></script>
	<script type ="text/javascript" src = "common.js"></script>
</head>
<body>

<?php include 'common_body.php'; ?>

<script type="text/javascript">

$("#map").on("contextmenu", false);
$("group_view").width("30%");

$("#map").mousedown(function(e) {
	close_group_view();
	close_add_user_view();
	close_create_group_view();
	mouse = true;
	mouseX = e.pageX;
	mouseY = e.pageY;
});

$(document).mouseup(function(e) {
	mouse = false;
});

$(document).mousemove(function(e) {
	if (!mouse) return;
	var dx = e.pageX - mouseX;
	var dy = e.pageY - mouseY;
	mouseX = e.pageX;
	mouseY = e.pageY;
	$("#map").offset({ top: $("#map").offset().top + dy, left: $("#map").offset().left + dx });
});

mode = "<?php echo($_GET['mode']); ?>";

if (mode == "admin") {
	$("#group_view_admin").css("display", "inline");
    $("#map").on("contextmenu", function(e) {
    	add_temp_point(mouseX, mouseY);
        showCreateGroupView(e.clientX, e.clientY);
    });
}
else {
	mode = "user";
}

</script>
</body>
</html>