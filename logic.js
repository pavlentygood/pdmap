var mode;
var x, y;
var mouseX, mouseY;
var mouse = false;
var group;
var groupId = 0;
var groupName = "";
var dist = 0;
var mapSize = 1;
var resize_animate = false;
var mapw = $("#map").width();
var maph = $("#map").height();
var psizeProc = 2.0;
var pointSize = mapw / 100.0 * psizeProc;
var minSize = 500;
var maxSize =  5000;
var resizeValue =  300;
var point_anim_status = false;
var move_speed = 3;

var groups = JSON.parse(request("pdmap/groups"));

groups.forEach(function(item) {
    item.users = JSON.parse(request("pdmap/users/" + item.Id));
    draw_point(item.Id, item.X, item.Y);
});

var kgblist = JSON.parse(request("pdmap/kgblist"));

kgblist.forEach(function(item) {
    $("#kgblist").append("<option value ='"+item.Id+"'>"+item.Name+"</option>");
});

function getGroup(id) {
    var gr;
    groups.forEach(function(item) {
        if (item.Id == id) {
            gr = item;
            return;
        }
    });
    return gr;
}

function getUser(gid, uid) {
    var u;
    getGroup(gid).users.forEach(function(item) {
        if (item.Id == uid) {
            u = item;
            return;
        }
    });
    return u;
}

function setPos(name, px, py) {
    $(name).css("left", (px/10-psizeProc/2) + "%");
    $(name).css("top",  (py/10-psizeProc/2) + "%");
}

function draw_point(id, px, py) {
    $("#map").append("<div class='point "+id+"' group_id='"+id+"'></div>");
    var p = ".point." + id;
    setPos(p, px, py);
    // var gr = getGroup(id);
    // var ofx = 0;
    // gr.users.forEach(function(u) {
    //     $(p).append("<img class='uimg" + u.Id + "' src='" + u.Img + "'>");
    //     var uimg = $(".uimg" + u.Id);
    //     uimg.css("left", (25*ofx++) + "%");
    // });
}

function drawUsers(gr) {
    $(".user").remove();
    gr.users.forEach(function(item) {
        $("#group_view").append("<div class='user "+item.Id+"' uid='"+item.Id+"'></div>");
        $(".user."+item.Id).append("<img src='"+item.Img+"'><div id='uname'>"+item.Name+"</div>");
    });
}

function showCreateGroupView(clientX, clientY) {
    $("#create_group_view").css("display", "inline-block");
    x = Math.round((clientX - $("#map").offset().left) * 10.0) / 10.0;
    y = Math.round((clientY - $("#map").offset().top) * 10.0) / 10.0;
    $("#xy").html("x: " + x + ", y: " + y);
}

function deleteUserFromGroup(gid, uid) {
    var gr = getGroup(gid);
    gr.users.forEach(function(item, i) {
        if (item.Id == uid) {
            gr.users.splice(i, 1);
            return;
        }
    });
}

function showGroupView() {
    $("#group_view").css("display", "inline-block");
    var gr = getGroup(groupId);
    groupName = gr.Name;
    $("#group_name").html(groupName);
    drawUsers(gr);
    $(".user").on("mousedown", function(e) {
        if (mode != "admin") return;
        if (confirm("Удалить человека из этой группы?")) {
            var uid = Number($(this).attr("uid"));
            request("pdmap/delete_user/" + uid + "/" + groupId);
            $(this).remove();
            deleteUserFromGroup(groupId, uid);
        }
    });
}

function show_add_user_view() {
    $("#add_user_view").css("display", "block");
    $("#add_user_view #group_name").html(groupName);
}

function close_add_user_view() {
    $("#user_id").val("");
    $("#add_user_view").css("display", "none");
}

function close_create_group_view() {
    delete_temp_point();
    $("#new_group_name").val("");
    $("#create_group_view").css("display", "none");
}

function close_group_view() {
    $("#group_view").css("display", "none");
    $(".user").remove();
}

function create_group() {
    var n = $("#new_group_name").val();
    var xx = Math.round(1000 * x / mapw);
    var yy = Math.round(1000 * y / maph);
    var id = request("pdmap/add_group/" + n + "/" + xx + "/" + yy);
    groups.push({Id:id, Name: n, X:xx, Y:yy});
    draw_point(id, xx, yy);
    close_create_group_view();
    $(".point").click(function(e) {
        point_click(this);
    });
}

function add_user() {
    var userId = Number($("#user_id").val());
    request("pdmap/add_user/" + userId + "/" + groupId);
    close_add_user_view();
    var gr = getGroup(groupId);
    gr.users = JSON.parse(request("pdmap/users/" + groupId));
    close_group_view();
    showGroupView();
}

function add_temp_point(xxx, yyy) {
    var s = mapw * 0.01;
    $("#map").append("<div id='temp_point'></div>");
    $("#temp_point").css("width", s+"px");
    $("#temp_point").css("height", s+"px");
    $("#temp_point").css("left", (xxx-s/2)+"px");
    $("#temp_point").css("top", (yyy-s/2)+"px");
}

function delete_group() {
    if (confirm("Удалить эту группу?")) {
        request("pdmap/delete_group/" + groupId);
        close_group_view();
        $(".point." + groupId).remove();
    }
}

function delete_temp_point() {
    $("#temp_point").remove();
}

function resize_map(a) {
    if (resize_animate) return;
    close_create_group_view();
    resize_animate = true;
    var anim_time = 200;
    var offs = $("#map").offset();
    var nx = (mapw+a) * offs.left / mapw;
    var ny = (maph+a) * offs.top / maph; 
    $("#map").animate({ width:"+="+a, height:"+="+a, left:nx, top:ny }, anim_time, function() {
        resize_animate = false;
        mapw = $("#map").width();
        maph = $("#map").height();
    });
}

function resize_up() {
    if ($("#map").width() < maxSize) {
        resize_map(resizeValue);
    }
}

function resize_down() {
    if ($("#map").width() > minSize) {
        resize_map(-resizeValue);
    }
}

function move_group() {
    $("#move_buttons").css("display", "block");
    group = getGroup(groupId);
}

function move_point(gr) {
	setPos(".point." + gr.Id, gr.X, gr.Y);
}

function move_up() {
	group.Y -= move_speed;
	move_point(group);
}

function move_right() {
	group.X += move_speed;
	move_point(group);
}

function move_down() {
	group.Y += move_speed;
	move_point(group);
}

function move_left() {
	group.X -= move_speed;
	move_point(group);
}

function show_point_animate(point) {
    if (point_anim_status) return;
    point_anim_status = true;
    var a = 6;
    var aa =  psizeProc;
    $(point).animate({width: a+"%", height: a+"%",  left: "-=2.5%",  top: "-=2.5%"}, 300, function() {
        $(point).animate({width: aa+"%", height: aa+"%", left: "+=2.5%",  top: "+=2.5%"}, 300, function() {
            point_anim_status =  false;
            if (!point.loaded) {
                show_point_animate(point);
            }
        });
    });
}

function point_click(point) {
    point.loaded = false;
    groupId = Number($(point).attr("group_id"));
    var gr = getGroup(groupId);
    point.loaded = true;
    show_point_animate(point);
    showGroupView();
}

$(".point").click(function(e) {
    point_click(this);
});