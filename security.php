<?php

include 'requests/token.php';
	
session_start();

 if (!isset($_SESSION['pdmap_admin']) || $_SESSION['pdmap_admin'] != $token) {
   	header("Location: auth_form.php");
 }

?>