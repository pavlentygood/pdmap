<?php

if ($_GET['mode'] == "admin") {
	include 'security.php';
}

?>

<div id="gray"></div>
<div id ="map"></div>
<div id ="fon"></div>

<div id="resize_buttons">
	<div id="resize_up"   onclick="javascript: resize_up  ();"></div><br>
	<div id="resize_down" onclick="javascript: resize_down();"></div>
</div>

<div id = "group_view">
	<div id="group_name"></div><br>
	<div id="group_view_admin">
		<button onclick="javascript: show_add_user_view();">Добавить</button>
		<button onclick="javascript: delete_group();">Удалить группу</button>
	</div>
	<br>
</div>

<div id = "create_group_view">
	<h3>Создание группы</h3>
	Позиция: <div id = "xy"></div>
	<br><br>
	Название группы:<br>
	<input type="text" id="new_group_name"><br><br>
	<button onclick="javascript: create_group();">Создать</button>
</div>

<div id = "add_user_view">
	<h3>Добавление человека</h3>
	Группа: <div id="group_name"></div><br><br>
	Человек:<br>
	<input id="user_id" type="text" list="kgblist"/>
	<datalist id="kgblist"></datalist>
	<button onclick="javascript: add_user();">Добавить</button>
</div>

<script type ="text/javascript" src = "logic.js"></script>