<?php

include 'requests/token.php';

if ($_POST['pass'] != $token) {
	echo("Ошибка входа. <br><a href='auth_form.php?mode=user'>Назад</a>");
	return;
}

session_start();

$_SESSION['pdmap_admin'] = $token;

ini_set('session.save_path', '/sessions/');
ini_set('session.gc_maxlifetime', 60 * 60 * 24);

header("Location: index.php?mode=admin");

?>