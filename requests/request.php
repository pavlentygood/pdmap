<?php

session_start();
$token = $_SESSION['pdmap_admin'];

$json = "{\"token\":\"$token\"}";
$text = urlencode($_GET['text']);

$options = array(
    'http' => array(
        'header'  => "Content-type: application/json\r\n",
        'method'  => 'POST',
        'content' => $json
    )
);

$context  = stream_context_create($options);
$result = file_get_contents("http://62.109.4.204:8123/$text", false, $context);

if ($result === FALSE) {
	echo "error";
}
else {
	echo $result;
}

?>