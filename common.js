
function request(addr) {
    var req = new XMLHttpRequest();
    req.open('GET', "requests/request.php?text=" + addr, false);
    req.send();
    return req.responseText;
}

function asyncRequest(addr, func) {
    var req = new XMLHttpRequest();
    req.open('GET', "requests/request.php?text=" + addr, true);
    
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if (req.status === 200) {
                func(req.responseText);
            }
            else {
                document.write("Ошибка " + req.statusText);
            }
        }
    };
    req.send();
}

function getDist(x1,x2,y1,y2) {
    return Math.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
}