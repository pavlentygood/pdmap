<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Карта ПД</title>
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<link rel="stylesheet" href = "style.css" type = "text/css">
	<link rel="stylesheet" href = "mobile_style.css" type = "text/css">
	<script type ="text/javascript" src = "jquery.js"></script>
	<script type ="text/javascript" src = "common.js"></script>
</head>
<body>

<?php include 'common_body.php'; ?>

<script type="text/javascript">

var waitCreateGroup = false;

$("#map").on("touchstart", function(e) {
	close_group_view();
	close_add_user_view();
	close_create_group_view();
	waitCreateGroup = true;
	setTimeout(function() {
		if (mode == "admin" && waitCreateGroup) {
			waitCreateGroup = false;
			add_temp_point(mouseX, mouseY);
			showCreateGroupView(mouseX, mouseY);
		}
	}, 2000);
	mouse = true;
	mouseX = e.changedTouches[0].pageX;
	mouseY = e.changedTouches[0].pageY;
	if(e.originalEvent.touches.length == 2) {
		var x1 = e.changedTouches[0].pageX;
		var y1 = e.changedTouches[0].pageY;
		var x2 = e.originalEvent.touches[1].pageX;
		var y2 = e.originalEvent.touches[1].pageY;
		dist = getDist(x1,x2,y1,y2);
	}
});

$(document).on("touchend", function(e) {
	mouse = false;
	waitCreateGroup = false;
	dist = getDist(x1,x2,y1,y2);
});

$(document).on("touchclose", function(e) {
	mouse = false;
	waitCreateGroup = false;
	dist = getDist(x1,x2,y1,y2);
});

$(document).on("touchmove", function(e) {
	if (!mouse) return;
	waitCreateGroup = false;
	var x1 = e.changedTouches[0].pageX;
	var y1 = e.changedTouches[0].pageY;

	if (e.originalEvent.touches.length == 1) {
		var dx = x1 - mouseX;
		var dy = y1 - mouseY;
		mouseX = x1;
		mouseY = y1;
		$("#map").offset({ top: $("#map").offset().top + dy, left: $("#map").offset().left + dx });
	}
	else if(e.originalEvent.touches.length == 2) {
		//var x2 = e.originalEvent.touches[1].pageX;
		//var y2 = e.originalEvent.touches[1].pageY;
		//var d = getDist(x1,x2,y1,y2);
		//var dd = d - dist;
		//dist = d;
		//$("#map").width($("#map").width() + dd*2);
		//$("#map").height($("#map").height() + dd*2);
		//var a = dd;
		//$("#map").animate({width:"+="+a, height:"+="+a, left:"-="+a/2, top:"-="+a/2}, 200);
	}
});

mode = "<?php echo($_GET['mode']); ?>";

if (mode == "admin") {
	$("#group_view_admin").css("display", "inline");
}
else {
	mode = "user";
}

</script>
</body>
</html>